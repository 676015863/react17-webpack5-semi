const fs = require('fs');
const Koa = require('Koa');
const Router = require('koa-router');
const koaStatic = require('koa-static');
const path = require('path');
const { renderToHTML, routes } = require('./ssr/ssr.js');
const Loadable = require('react-loadable');
const { matchRoutes } = require('react-router-config');

// 配置文件
const config = {
  port: 3030
};

function getIndexHTML() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, './static/index.html'), 'utf8', function (err, data) {
      if (err) {
        reject();
        return console.log(err);
      }
      resolve(data);
    });
  });
}

async function main() {
  // 实例化 koa
  const app = new Koa();

  // 静态资源
  app.use(
    koaStatic(path.join(__dirname, './static'), {
      maxage: 365 * 24 * 60 * 1000,
      index: 'root'
      // 这里配置不要写成'index'就可以了，因为在访问localhost:3030时，不能让服务默认去加载index.html文件，这里很容易掉进坑。
    })
  );

  const indexHTML = await getIndexHTML();

  // 设置路由
  app.use(
    new Router()
      .get('*', async (ctx, next) => {
        const [routeAll, routeTarget] = matchRoutes(routes, ctx.request.path);
        if (!routeTarget) {
          ctx.response.body = ctx.request.path; // shtml.replace('{{root}}', '404');
        } else {
          // console.log('routeTarget>>>>>>>', ctx.request.path, routeTarget);
          // 如果没有做SSR，应该直接显示local App模块
          let shtml = 'loading',
            scripts = '<script>window.onload=coverServerDOM</script>';

          // 选择性的做ssr
          if (routeTarget.route.ssr) {
            const data = await renderToHTML(routeTarget.route, {
              location: ctx.request.path,
              context: { ...routeTarget.match }
            });
            shtml = data.shtml || 'loading';
            scripts = data.scripts;
          }
          ctx.response.type = 'html'; //指定content type
          ctx.response.body = indexHTML.replace('{{root}}', shtml).replace('{{scripts}}', scripts);
        }
      })
      .routes()
  );

  Loadable.preloadAll().then(() => {
    app.listen(config.port, function () {
      console.log('服务器启动，监听 port： ' + config.port + '  running~');
    });
  });
}

main();
