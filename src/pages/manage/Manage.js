import React, { Component } from 'react';
import './manage.less';
import { inject, observer } from 'mobx-react';
import { Button } from '@douyinfe/semi-ui';
import { pubsub } from '../../utils';

@inject('user')
@observer
class Manage extends Component {
  startPageLoading = () => {
    pubsub.publish('pageLoading', {
      start: true
    });
  };

  endPageLoading = () => {
    pubsub.publish('pageLoading', {
      end: true
    });
  };

  render() {
    console.log(this.props.user.info);
    return (
      <div>
        登录后的页面
        <img style={{ width: 100 }} src={this.props.user.info.avatarUrl} alt="" />
        <Button onClick={this.startPageLoading}>page loading start</Button>
        <Button onClick={this.endPageLoading}>page loading end</Button>
      </div>
    );
  }
}
export default Manage;
