import { dynamic } from '../../../src/utils';
const Home = dynamic(import('./Home'));
const Home2 = dynamic(import('./Home2'));

const routes = [
  {
    path: 'home',
    ssr: true,
    exact: true,
    component: Home
  },
  {
    path: 'home2/:id',
    ssr: true,
    exact: true,
    component: Home2
  }
];

export { routes };
