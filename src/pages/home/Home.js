import React, { Component } from 'react';
import './home.less';
import { inject, observer } from 'mobx-react';
import { config } from '../../config';
import { util } from '../../utils';
import { Form, Toast, Button, Tag, Space } from '@douyinfe/semi-ui'; // ...
import { userService } from '../../server';
import { Routes, Route, Link } from 'react-router-dom';

function ajaxTest() {
  console.log('请求数据');
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(
        new Array(20).fill(1).map((d, i) => {
          return { id: i };
        })
      );
    }, 1000);
  });
}

// ssr 配置
// console.log(process.env.NODE_ENV);
@inject('user')
@observer
class Home extends Component {
  // 服务端数据组装
  static ssrData = async routeParams => {
    console.warn('ajax异步加载数据', routeParams);
    const ssrRes = await ajaxTest();
    return { ssrRes };
  };

  constructor(props) {
    super(props);
    this.state = {
      list: props.ssrRes || window.ssrData_Home?.ssrRes || [],
      captcha: userService.getCaptcha()
    };
    window.coverServerDOM();
  }

  componentDidMount() {
    // console.log('user info', this.props.user.info);
    if (this.props.user.info) {
      // history.push(config.loginLinkTo);
    }

    if (!window.ssrData_Home) {
      Home.ssrData().then(res => {
        this.setState({ list: res.ssrRes });
      });
    }
  }

  handleSubmit = async values => {
    // Toast.info('表单已提交');
    const res = await userService.login(values);
    console.log(values, res);
    if (res) {
      await userService.getUserDetail();
      // history.push('/manage');
    }
  };

  render() {
    console.warn('state.list', this.state.list); // ...
    return (
      <>
        <div className="home">
          <div>
            <h1>SSR</h1>
            <Link to="/home2/2">HOME</Link>
            <div>
              {this.state.list.map(d => {
                return (
                  <Tag style={{ margin: 5 }} key={d.id}>
                    {d.id}
                  </Tag>
                );
              })}
            </div>
          </div>
        </div>
        <div className="home">
          <Form onSubmit={values => this.handleSubmit(values)} style={{ width: 400 }}>
            {({ formState, values, formApi }) => (
              <>
                <Form.Input
                  field="username"
                  label="PhoneNumber"
                  initValue="13551301693"
                  style={{ width: '100%' }}
                  placeholder="Enter your phone number"></Form.Input>
                <Form.Input
                  field="password"
                  label="Password"
                  type="password"
                  style={{ width: '100%' }}
                  placeholder="Enter your password"></Form.Input>
                <Form.Input
                  field="captchaCode"
                  label="Captcha"
                  style={{ width: '100%' }}
                  placeholder="Enter your captchaCode"></Form.Input>
                <img
                  onClick={() => this.setState({ captcha: userService.getCaptcha() })}
                  style={{ width: 86, height: 34 }}
                  src={this.state.captcha}
                  alt=""
                />
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  <Button htmlType="reset">reset</Button>
                  <Button disabled={!(values.username && values.password)} htmlType="submit" type="tertiary">
                    Log in
                  </Button>
                </div>
              </>
            )}
          </Form>
        </div>
      </>
    );
  }
}
export default Home;
