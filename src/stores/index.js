import { layout } from './layout';
import { user } from './user';

const stores = {
  layout,
  user
};

export { stores };
