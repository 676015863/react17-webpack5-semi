// import { routes as authorizationRoutes } from './pages/authorization';

import { routes as homeRoutes } from './pages/home';
import { routes as manageRoutes } from './pages/manage';
import { dynamic } from './utils';
// 管理页面
const NotFound = dynamic(import('./components/not-found'));
const IndexLayout = dynamic(import('./layout/index-layout'));
const AuthLayout = dynamic(import('./layout/auth-layout'));

const routes = [
  {
    path: '/manage',
    exact: false,
    component: AuthLayout,
    meta: { auth: true },
    routes: [...manageRoutes, { path: '*', exact: false, component: NotFound }]
  },
  {
    path: '/',
    exact: false,
    component: IndexLayout,
    meta: { auth: false },
    routes: [
      ...homeRoutes, // 首页
      // { path: '*', exact: false, component: NotFound }
    ]
  }
];

export { routes };
