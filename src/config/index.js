// 调试模式
window.debug = true;

// 配置
const config = {
  secretKey: '1QiLCJh', // 加密
  apiHost: '/api/v1',
  prefix: 'px', // 项目前缀，用于设置localStroage的名称
  unLoginLinkTo: '/home', // 访问未登录页面跳转
  loginLinkTo: '/manage', // 登录后页面跳转
  basename: '' // history路由前缀
};

// 生产环境参数
if (process.env.NODE_ENV !== 'development') {
  config.apiHost = '/api/v1';
  window.debug = false;
}

export { config };
