import Intl from './Intl';
import { locals, language } from './language';

export { locals, Intl, language };
