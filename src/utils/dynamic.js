import Loadable from 'react-loadable';
import React from 'react';
import { Spin } from '@douyinfe/semi-ui';

function Loading({ error }) {
  if (error) {
    console.error(error);
    return <div>页面加载错误</div>;
  }
  return <Spin />;
}
export function dynamic(compPromise) {
  return Loadable({
    loader: () => {
      return compPromise;
    },
    loading: Loading
  });
}
