import './auth-layout.less';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { config } from '../../config';
import { Spin } from '@douyinfe/semi-ui';
import PageLoading from '../../components/page-loading';

@inject('user')
@observer
class AuthLayout extends Component {

  componentDidMount() {
    // 未登录状态直接返回首页
    if (this.props.parentRoute.meta.auth && !this.props.user.token) {
      window.routerHistory.push(config.unLoginLinkTo);
    }
  }

  render() {
    const { match, routes = [] } = this.props;
    const parentPath = match.path;
    const { info } = this.props.user;
    if (!info) {
      return <Spin />;
    }
    return (
      <>
        <PageLoading />
        <Switch>
          {routes.map(route => {
            const routeKey = parentPath + route.path;
            return (
              <Route
                key={routeKey}
                path={routeKey}
                render={props => <route.component {...props} routes={route.routes || []} />}
              />
            );
          })}
        </Switch>
      </>
    );
  }
}

export default AuthLayout;
