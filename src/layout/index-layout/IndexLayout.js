import './index-layout.less';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import PageLoading from '../../components/page-loading';

class IndexLayout extends Component {
  render() {
    const { routes = [], match } = this.props;
    const parentPath = match.path;
    return (
      <>
        <PageLoading />
        <Switch>
          {routes.map(route => {
            const routeKey = parentPath + route.path;
            return (
              <Route
                key={routeKey}
                path={routeKey}
                render={props => <route.component {...props} routes={route.routes || []} />}
              />
            );
          })}
        </Switch>
      </>
    );
  }
}

export default IndexLayout;
