import './style.less';
import './less/icon.less';
import './utils/pubsub';
import './less/initialize.less';
import { Provider } from 'mobx-react';
import React from 'react';
import Routers from './Routers';
import { render } from 'react-dom';
import { stores } from './stores'; // ...
import { BrowserRouter } from 'react-router-dom'; // 路由
import { routes } from './routes.config';
// console.log(process.env.NODE_ENV);

if (process.env.NODE_ENV === 'development') {
  window.coverServerDOM();
}

// 路由
render(
  <Provider {...stores}>
    <Routers Router={BrowserRouter} routes={routes} />
  </Provider>,
  document.getElementById('App')
);

if (module.hot) {
  module.hot.accept();
}
