import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'; // 路由
import { util, pubsub } from './utils';
import { userService } from './server';
import { config } from './config';
import { stores } from './stores';

class Routers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyid: util.randomID()
    };
    this.routerRef = React.createRef();
  }

  componentDidMount() {
    const { history } = this.routerRef.current;
    window.routerHistory = history;

    const p = window.location.pathname.split('/')[1];
    const [data] = this.props.routes.filter(route => route.path === '/' + p);

    // 如果url存在token，先设置token参数，再获取用户数据
    let token = util.getUrlQuery('token');
    if (token) {
      // 去掉url对应的token参数
      window.history.pushState(null, null, util.delUrlParam('token'));
      token = decodeURI(token);
      stores.user.setToken(token);
    }

    // 需要登录
    if (data && data?.meta?.auth) {
      userService.getUserDetail();
      console.log('需要登录，更新用户信息'); //
    }

    pubsub.subscribe('setLanguage', () => {
      this.setState({ keyid: util.randomID() });
    });
  }

  componentWillUnmount() {
    pubsub.unsubscribe('setLanguage');
  }

  render() {
    const { Router, routes, ...otherProps } = this.props;
    return (
      <Router key={this.state.keyid} ref={this.routerRef} basename={config.basename} {...otherProps}>
        <Switch>
          <Redirect exact from="/" to="/home" />
          {routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact} // exact的值为bool型，为true时表示严格匹配，为false时为正常匹配
                render={props => <route.component {...props} routes={route.routes} parentRoute={route} />}
              />
            );
          })}
        </Switch>
      </Router>
    );
  }
}

export default Routers;
