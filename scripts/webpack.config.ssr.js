const { resolve, srcPath, version, hash, distPath } = require('./config');

// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const baseConfig = require('./webpack.config.base');
const { merge } = require('webpack-merge');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const LessPluginFunctions = require('less-plugin-functions');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const TerserPlugin = require('terser-webpack-plugin');
const { SemiWebpackPlugin } = require('./SemiWebpackPlugin');
const webpack = require('webpack');

module.exports = merge(baseConfig, {
  devtool: false,
  target: 'node',
  externalsPresets: { node: true }, // in order to ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()],
  entry: {
    ssr: resolve('../src/ssr.js') // 主网站入口
  },
  output: {
    publicPath: '/',
    path: distPath,
    filename: `ssr/[name].js`,
    libraryTarget: 'commonjs2'
  },
  // mode: 'production',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: [srcPath, resolve('../node_modules')],
        use: ['babel-loader']
      },
      {
        test: /\.(css|less)$/,
        include: [srcPath, resolve('../node_modules')],
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                javascriptEnabled: true,
                plugins: [new LessPluginFunctions()]
              }
            }
          }
        ]
      }
    ]
  },
  // optimization: {
  //   minimizer: [
  //     new OptimizeCSSAssetsPlugin({}),
  //     new TerserPlugin({
  //       terserOptions: {
  //         compress: {
  //           warnings: false, // 去除warning警告
  //           // drop_debugger: true,// 发布时去除debugger语句
  //           // drop_console: true, // 发布时去除console语句
  //           pure_funcs: ['console.log'] // 配置发布时，不被打包的函数
  //         }
  //       }
  //     })
  //   ],
  //   splitChunks: {
  //     cacheGroups: {
  //       // 出现多次的js包单独打包成common.js
  //       commons: {
  //         name: 'common',
  //         priority: 10, // 优先级
  //         // test: /react|mobx|antd|moment/,
  //         test: /node_modules/,
  //         chunks: 'initial',
  //         // minSize: 0, // 默认小于30kb不会打包
  //         minChunks: 2 // 引用1次就要打包出来
  //       }
  //     }
  //   }
  // },
  plugins: [
    // new webpack.ProvidePlugin({
    //   Buffer: ['buffer', 'Buffer']
    // }),
    new webpack.DefinePlugin({
      __RUNNING_ENV__: JSON.stringify('ssr')
    }),
    new SemiWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: `assets/css/[name].${version}${hash}.css`
    }),
    // new CleanWebpackPlugin({
    //   root: __dirname.replace('scripts', 'dist/ssr')
    // }),
    new FileManagerPlugin({
      events: {
        onEnd: {
          copy: [
            { source: resolve('../ssr/server.js'), destination: resolve('../dist/server.js') },
            { source: resolve('../ssr/index.js'), destination: resolve('../dist/index.js') }
          ]
        }
      }
    })
  ]
});
